\contentsline {section}{\tocsection {}{1}{Overview}}{1}{section.1}
\contentsline {section}{\tocsection {}{2}{Object}}{2}{section.2}
\contentsline {section}{\tocsection {}{3}{Game Contents}}{2}{section.3}
\contentsline {section}{\tocsection {}{4}{Set Up}}{2}{section.4}
\contentsline {section}{\tocsection {}{5}{Gameplay}}{3}{section.5}
\contentsline {subsection}{\tocsubsection {}{5.1}{Election}}{3}{subsection.5.1}
\contentsline {subsection}{\tocsubsection {}{5.2}{Appointment Procedure}}{5}{subsection.5.2}
\contentsline {subsection}{\tocsubsection {}{5.3}{Executive Action}}{6}{subsection.5.3}
\contentsline {subsection}{\tocsubsection {}{5.4}{Presidential Powers}}{6}{subsection.5.4}
\contentsline {section}{\tocsection {}{6}{Strategy Notes}}{7}{section.6}
\contentsline {section}{\tocsection {}{7}{Credits \& License}}{8}{section.7}
