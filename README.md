# The Institute #
## A social deduction game based on science as we know it. ##

Science is threatened by career-obsessed and egoistic
researchers. You are part of an ever-fading community
of honest scientists fighting to safe
science. A new institute has been created and it is your
job to hire the permanent positions. Some of the applicants
are honest scientists like you, but many are not, and the same is true
among your colleagues in the board of your research 
union. You have to fight hard to make sure the institute
ends up in the hands of respectable and trustworthy scientists
who want to further science rather than their own good.

### How do I get started? ###

* The rules of the game are in the folder 'Rules' (PDF file).
* Print all cards in the 'Print A4 PDF' folder.

### Contribution guidelines ###

* Contributions and improvements welcome!
* Modify playboard, cards, rules, etc. in corresponding folder.

### License & Credits ###

This game is licensed under a Creative
Commons Attribution-NonCommercial-ShareAlike 4.0
International License. It is based on the game 'Secret Hitler'.